<?php

/**
 * Set up theme options on activation
 */

function vue_wordpress_setup()
{

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'custom-logo', array(
        'height' => 160,
        'width' => 160,
    ) );

    register_nav_menus( array(
        'main' => 'Main Menu',
    ) );

}

add_action( 'after_setup_theme', 'vue_wordpress_setup' );

/**
 * Load scripts and styles
 */

function vue_wordpress_scripts()
{
    // Styles
    //wp_enqueue_style( 'style.css', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style('vue_wordpress.css', get_template_directory_uri() . '/dist/vue-wordpress.css');
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Oranienbaum', false );
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/aai5gfb.css', false ); 

    // Scripts

    // Enable For Production - Disable for Development
    //wp_enqueue_script('vue_wordpress.js', get_template_directory_uri() . '/dist/vue-wordpress.js', array(), null, true);

    // Enable For Development - Remove for Production
    wp_enqueue_script( 'vue_wordpress.js', 'http://localhost:8080/vue-wordpress.js', array(), false, true );
}

add_action( 'wp_enqueue_scripts', 'vue_wordpress_scripts' );

/**
 * Declare REST API Data Localizer dependency
 */

if ( !class_exists( 'RADL' ) ) {
    add_action( 'admin_notices', function () {
        echo '<div class="error"><p>REST API Data Localizer not activated. To use this theme go to <a href="' . esc_url( admin_url( 'plugins.php' ) ) . '">plugins</a> to download and/or activate REST API Data Localizer.</p></div>';
    } );
    return;
}

/**
 * Initialize REST API Data Localizer
 */

new RADL( '__VUE_WORDPRESS__', 'vue_wordpress.js', array(
    'routing' => RADL::callback( 'vue_wordpress_routing' ),
    'state' => array(
        //'categories' => RADL::endpoint( 'categories'),
        //'media' => RADL::endpoint( 'media' ),
        'menus' => RADL::callback( 'vue_wordpress_menus' ),
        'page' =>  (object) [],
        'post' => (object) [],
        //'tags' => RADL::endpoint( 'tags' ),
        'users' =>  (object) [],
        'site' => RADL::callback( 'vue_wordpress_site' ),
        'services' => (object) [],
        'projects' => (object) [],
        'vacancies' => (object) [],
        'faq' => (object) [],
        'highlights' => (object) [],


    ),
) );

/**
 * REST API Data Localizer callbacks
 */

function vue_wordpress_routing()
{
    $routing = array(
        'category_base' => get_option( 'category_base' ),
        'page_on_front' => null,
        'page_for_posts' => null,
        'permalink_structure' => get_option( 'permalink_structure' ),
        'show_on_front' => get_option( 'show_on_front' ),
        'tag_base' => get_option( 'tag_base' ),
        'url' => get_bloginfo( 'url' )
    );

    if ( $routing['show_on_front'] === 'page' ) {
        $front_page_id = get_option( 'page_on_front' );
        $posts_page_id = get_option( 'page_for_posts' );

        if ( $front_page_id ) {
            $front_page = get_post( $front_page_id );
            $routing['page_on_front'] = $front_page->post_name;
        }

        if ( $posts_page_id ) {
            $posts_page = get_post( $posts_page_id );
            $routing['page_for_posts'] = $posts_page->post_name;
        }

    }

    return $routing;
}

function vue_wordpress_menus()
{
    $menus = array();
    // $locations is an array where ([NAME] = MENU_ID);
    $locations = get_nav_menu_locations();

    foreach ( array_keys( $locations ) as $name ) {
        $id = $locations[$name];
        $menu = array();
        $menu_items = wp_get_nav_menu_items( $id );

        foreach ( $menu_items as $i ) {

            array_push( $menu, array(
                'id'      => $i->ID,
                'parent'  => $i->menu_item_parent,
                'target'  => $i->target,
                'content' => $i->title,
                'title'   => $i->attr_title,
                'url'     => $i->url,
            ) );

        }

        $menus[$name] = $menu;
    }

    return $menus;
}

function vue_wordpress_site()
{
    return array(
        'description' => get_bloginfo( 'description' ),
        'docTitle' => '',
        'loading' => false,
        'logo' => get_theme_mod( 'custom_logo' ),
        'name' => get_bloginfo( 'name' ),
        'posts_per_page' => get_option( 'posts_per_page' ),
        'url' => get_bloginfo( 'url' )
    );

}

/**
 * In template functions
 */

function vue_wordpress_min_read( $content )
{
    $length = count( explode( ' ', $content ) ) + 1;
    $time = $length / 200;

    if ( is_float( $time ) ) {
        $time = ceil( $time );
    }
   
    return $time . 'min read';
}
function create_post_type() { // создаем новый тип записи
    register_post_type( 'projects', // указываем названия типа
        array(
            'labels' => array(

                'name' => __( 'Projects' ), // даем названия разделу, для панели управления
                'singular_name' => __( 'Project' ) // даем названия одной записи
            ),
            'publicly_queryable' => true,
            'taxonomies' => array('post_tag'),
            'supports' => array('title', 'thumbnail' ),
            'menu_icon'           => 'dashicons-welcome-view-site',
            'public' => true,
            'menu_position' => 5, // указываем место в левой баковой панели
            'rewrite' => array('slug' => 'projects'), // указываем slug для ссылок например: http://mysite/reviews/
            'show_in_rest' => true

            
        )
    );
    register_post_type( 'Services', // указываем названия типа
        array(
            'labels' => array(

                'name' => __( 'Services' ), // даем названия разделу, для панели управления
                'singular_name' => __( 'Services' ) // даем названия одной записи
            ),
            'publicly_queryable' => true,
            'taxonomies' => array('post_tag'),
            'supports' => array('title', 'thumbnail' ),
            'menu_icon'           => 'dashicons-format-gallery',
            'public' => true,
            'menu_position' => 5, // указываем место в левой баковой панели
            'rewrite' => array('slug' => 'services'), // указываем slug для ссылок например: http://mysite/reviews/
            'show_in_rest' => true
        )
    );
    register_post_type( 'FAQ', // указываем названия типа
        array(
            'labels' => array(

                'name' => __( 'F.A.Q.' ), // даем названия разделу, для панели управления
                'singular_name' => __( 'FAQ' ) // даем названия одной записи
            ),
            'publicly_queryable' => true,
            'taxonomies' => array('post_tag'),
            'supports' => array('title', 'editor' ),
            'menu_icon'           => 'dashicons-editor-ol',
            'public' => true,
            'menu_position' => 5, // указываем место в левой баковой панели
            'rewrite' => array('slug' => 'services'),// указываем slug для ссылок например: http://mysite/reviews/
            'show_in_rest' => true
        )
    );
    register_post_type( 'vacancies', // указываем названия типа
        array(
            'labels' => array(

                'name' => __( 'Vacancies' ), // даем названия разделу, для панели управления
                'singular_name' => __( 'vacancy' ) // даем названия одной записи
            ),
            'publicly_queryable' => true,
            'taxonomies' => array('post_tag'),
            'supports' => array('title'),
            'menu_icon'           => 'dashicons-editor-ol',
            'public' => true,
            'menu_position' => 5, // указываем место в левой баковой панели
            'rewrite' => array('slug' => 'software'), // указываем slug для ссылок например: http://mysite/reviews/
            'show_in_rest' => true
        )
    );
    register_post_type( 'Highlights', // указываем названия типа
        array(
            'labels' => array(

                'name' => __( 'Highlights' ), // даем названия разделу, для панели управления
                'singular_name' => __( 'Highlights' ) // даем названия одной записи
            ),
            
            'publicly_queryable' => true,
            'taxonomies' => array('post_tag'),
            'supports' => array('author','title', 'editor', 'thumbnail','page-attributes' ),
            'menu_icon'           => 'dashicons-editor-ol',
            'public' => true,
            'menu_position' => 5, // указываем место в левой баковой панели
            'rewrite' => array('slug' => 'highlights'), // указываем slug для ссылок например: http://mysite/reviews/
            'show_in_rest' => true
        )
    );

}
add_action( 'init', 'create_post_type' ); // инициируем добавления типа



add_action('wp_ajax_contact_form', 'contact_form');
add_action( 'wp_ajax_nopriv_contact_form', 'contact_form' );

function contact_form(){
  $name     = $_POST['name'];
  $phone    = $_POST['email'];
  $email    = $_POST['phone'];
  $company    = $_POST['company'];
  $details  = $_POST['details'];
  $msg = "Name: " .$name  .
         "\nEmail: " . $email .
         "\nPhone: " . $phone .
         "\nCompany: " . $company .   
        "\nDetails: " . $details;
  mail("igor@inshadow.agency","Inshadow Agency Contact Form",$msg);
  echo  $msg;
  wp_die();
}