export default {
  menu: state => ({ name }) =>{
    return state.menus[name]
  },
  singleBySlug: state => ({ type, slug }) => {
    return state[type][slug]
  },
  collectionByType: state => ({ type }) => {
    return state[type];
  },
  fields: state =>({type, slug}) =>{
    return state[type][slug]
  }
}