import Vue from 'vue'

export default {
  ADD_ITEM(state, { type, item }) {
    Vue.set(state[type], item.slug, item)
  },
  ADD_COLLECTION(state,{ type, collection }){
    collection.forEach(item => {
      Vue.set(state[type], item.slug, item)
    });
  },
  SET_LOADING(state, loading) {
    state.site.loading = loading
  },
}