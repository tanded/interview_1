import axios from 'axios'

const { url } = __VUE_WORDPRESS__.routing


const ajax = axios.create(
  {
    baseURL: `${url}/wp-content/plugins/cached-api/data/`,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
)

export const fetchSingle = ({ type, params }) => {
  return ajax.get(`/${type}/_${params.slug}.json`)
}
export const fetchByType = ({ type }) => {
  return ajax.get('_' + type +".json");
}
export default {
  getSingleBySlug({ getters, commit }, { type, slug, showLoading = false }) {
    if ( ! getters.singleBySlug({ type, slug }) ) {
      if (showLoading) {
        commit('SET_LOADING', true)
      }
      return fetchSingle({ type, params: { slug } }).then(({ data }) => {
        let item = data;
        commit('ADD_ITEM', { type, item })
        /*if (showLoading) {
          commit('SET_LOADING', false)
        }*/
        return item
      })
    }
  },
  getItems({ getters, commit }, { type, showLoading = false }) {
      return fetchByType({ type }).then((  {data}  )=>{
        commit('ADD_COLLECTION', { type, collection: data })
        return data
      })
  },
}