import Home from '@/pages/Home'
import About from '@/pages/About'
import Services from '@/pages/Services'
import Projects from '@/pages/Projects'
import Blog from '@/pages/Blog'
import Contact from '@/pages/Contact'
import singlePost from '@/pages/singlePost'
import singleProject from '@/pages/singleProject'


const staticRoutes = [
  {
    path: '/',
    component: Home,
    name: 'Home',
    props: {menu: true},
  },
  {
    path: '/about',
    component: About,
    name: 'About',
    props: {menu: false},
  },
  {
    path: '/services',
    component: Services,
    name: 'Services',
    props: {menu: false},
  },
  {
    path: '/projects',
    component: Projects,
    name: 'Projects',
    props: {menu: true},
  },
  {
    path: '/blog',
    component: Blog,
    name: 'Blog',
    props: {menu: true},
  },
  {
    path: '/contact',
    component: Contact,
    name: 'Contact',  
    props: {menu: false},
  }  
]
const dynamicRoutes = [
  {
    path: '/blog/:slug',
    component: singlePost,
    name: 'Single Post',
    props: {menu: false},
  },
  {
    path: '/projects/:slug',
    component: singleProject,
    name: 'Single Project',
    props: {menu: false},
  },
]
export default [
  ...staticRoutes,
  ...dynamicRoutes
]