<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Inshadow is a Digital Creative Agency in London, that focuses at web design, user experience design, website, frontend and backend development.">
    <meta name="keywords" content="Inshadow Agency, Creative Agency, London, United Kingdom, IT, Information Technology, Digital Development, Web Design, Web Agency, London design, Front-End development, Back-end, Corporate Identity, Branding, App development, App design, UI / UX Design, UXD, User Experience design, User Interface, UI Design, inshadow agency, inshadow, Woo-Commerce, Woo Commerce, Wordpress, Vue-js, Sketch, Java Script development, Pixel perfect, Html, CSS, Full-stack, Full stack agency, professional development, Unique design, PWA ready, Reactive apps, PWA, e-commerce, Internet shop, Android, Safari, Google Chrome, adaptation, deployment, API, Data Base, App store, Apple store, iOS, Systems, IT solutions, in shadow agency, web development, web development London, Software development, Design Foundation, B2B, B2C, Business partners, Marketing, Sales, Conversion, fast delivery, Magento">
    <?php wp_head(); ?>
    <link rel="icon" type="image/x-icon" href="/wp-content/themes/vuepress/src/favicon.png">
</head>

<body>

    <div id="vue-wordpress-app" class="container">

